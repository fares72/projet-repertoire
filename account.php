<?php

require 'inc/functions.php';
logged_only();/*on verifie que l'utilisateur est connecté*/

if (!empty($_POST)){
    if (empty($_POST['password']) || $_POST['password'] != $_POST['password_confirm']){
        $_SESSION['flash']['danger'] = 'Les mots de passes de corréspondent pas';
    }else{
        $user_id = $_SESSION['auth']->id;
        $password= password_hash($_POST['password'], PASSWORD_BCRYPT);

        require_once 'inc/db.php';
        $pdo->prepare('UPDATE users SET password = ?')->execute([$password]);

        $_SESSION['flash']['success'] = 'Votre mot de passe a bien été mis a jour';

    }
}
require 'inc/header.php';
?>


<div class="col-md-4 mx-auto">

<h1>Bonjour <?= $_SESSION['auth']->username; ?></h1>

<form action="" method="post">
    <div class="form-group">
        <input class="form-control" type="password" name="password" placeholder="Changer de mot de passe">
    </div>
    <div class="form-group">
        <input class="form-control"type="password" name="password_confirm" placeholder="Confirmer votre mot de passe">
    </div>

    <button class="btn btn-primary">Changer mon mot de passe </button>
</form>
</div>
<?php require 'inc/footer.php'; ?>
