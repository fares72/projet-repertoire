<?php
include('inc/db.php');
require 'inc/functions.php';
logged_only();
$upload_dir = 'uploads/';

if (isset($_GET['delete'])) {
    $id = $_GET['delete'];
    $req = $pdo->prepare("DELETE FROM contacts WHERE id = :id");
    $req->execute([
        'id' => $_GET['delete'],
    ]);
    header('location:index.php');
}
?>

<?php require 'inc/header.php' ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div>
                        <a id="btn_add" class="btn btn-success" href="create.php"><i class="fa fa-user-plus"></i></a>

                    </div>
                </div>
                <div class="row row-cols row-cols-md-3 g-4 p-3 ">
                    <?php
                    $req = $pdo->prepare('SELECT * FROM contacts WHERE user_id= :user_id');
                    $req->execute([
                        'user_id' => $_SESSION['auth']->id,
                    ]);
                    while ($contact = $req->fetch(PDO:: FETCH_ASSOC)) {
                        ?>
                        <div class="col">
                            <div class="card h-100">
                                <img src="<?php echo $upload_dir . $contact['image'] ?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $contact['name'] ?></h5>
                                    <p class="card-text"><?php echo $contact['contact'] ?></p>
                                    <p class="card-text"><small
                                                class="text-muted"><?php echo $contact['email'] ?></small>
                                    </p>

                                    <button class="accordion"><i class="bi bi-arrow-down-square"></i></button>
                                    <div class="liens panel">
                                        <a href="show.php?id=<?php echo $contact['id'] ?>" class="btn btn-info"><i
                                                    class="fa fa-eye"></i></a>
                                        <a href="edit.php?id=<?php echo $contact['id'] ?>"
                                           class="btn btn-warning"><i
                                                    class="fa fa-user-edit"></i></a>
                                        <a href="index.php?delete=<?php echo $contact['id'] ?>"
                                           class="btn btn-danger"
                                           onclick="return confirm('Voulez vous vraiment supprimer ce contact ?')">
                                            <i class="fa fa-trash-alt"></i></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require 'inc/footer.php' ?>
