<?php
require 'inc/header.php'
?>

<div class="p-5 mb-4 bg-light rounded-3 col-8">
    <div class="container-fluid py-3">
        <h1 class="display-5 fw-bold">Mes Contacts</h1>
        <p class="col-md-8 fs-4">Mes contact est une application open source qui permet d'ajouter des contacts. Pour cela il vous suffit de vous inscrire et c'est parti !</p>
        <a target="_blank" href="https://gitlab.com/fares72/projet-repertoire" class="btn btn-primary btn-lg" type="button">Code source</a>
    </div>
</div>

<?php require 'inc/footer.php'?>
