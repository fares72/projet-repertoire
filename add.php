<?php
require_once('inc/db.php');
require_once 'inc/functions.php';
logged_only();/*on verifie que l'utilisateur est connecté*/

$upload_dir = 'uploads/';


if (isset($_POST['Submit'])) {
    $name = $_POST['name'] = htmlentities($_POST['name']);
    $contact = $_POST['contact'] = htmlentities($_POST['contact']);
    $email = $_POST['email'] = htmlentities($_POST['email']);

    $imgName = $_FILES['image']['name'];
    $imgTmp = $_FILES['image']['tmp_name'];
    $imgSize = $_FILES['image']['size'];

        $imgExt = strtolower(pathinfo($imgName, PATHINFO_EXTENSION));

        $allowExt = array('jpeg', 'jpg', 'png', 'gif');

        $userPic = time() . '_' . rand(1000, 9999) . '.' . $imgExt;


                move_uploaded_file($imgTmp, $upload_dir . $userPic);


    if (isset($_POST['name'])) {
        $req = $pdo->prepare("insert into contacts(`name`, `contact`, `email`,`image`, `user_id`)
					values(:name, :contact, :email, :image, :user_id)");
        $req->execute(
            array('name' => $_POST['name'],
                'contact' => $_POST['contact'],
                'email' => $_POST['email'],
                'image' => $userPic,
                'user_id' => $_SESSION['auth']->id,
            )
        );
        header("Location: index.php");
    }
}
