<?php
require_once('inc/db.php');
require 'inc/functions.php';
logged_only();

$upload_dir = 'uploads/';

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $req = $pdo->prepare("SELECT * FROM contacts WHERE id=:id");
    $req->execute(['id' => $id]);
    $contact=$req->fetch(PDO::FETCH_ASSOC);
}

if(isset($_POST['Submit'])){
    $name = $_POST['name'] = htmlentities($_POST['name']);
    $contact = $_POST['contact'] = htmlentities($_POST['contact']);
    $email = $_POST['email'] = htmlentities($_POST['email']);

    $imgName = $_FILES['image']['name'];
    $imgTmp = $_FILES['image']['tmp_name'];
    $imgSize = $_FILES['image']['size'];

    if($imgName){

        $imgExt = strtolower(pathinfo($imgName, PATHINFO_EXTENSION));

        $allowExt  = array('jpeg', 'jpg', 'png', 'gif');

        $userPic = time().'_'.rand(1000,9999).'.'.$imgExt;

        if(in_array($imgExt, $allowExt)){

            if($imgSize < 5000000){
                unlink($upload_dir.$contact['image']);
                move_uploaded_file($imgTmp ,$upload_dir.$userPic);
            }else{
                $errorMsg = 'Image too large';
            }
        }else{
            $errorMsg = 'Please select a valid image';
        }
    }else{

        $userPic = $contact['image'];
    }

    if (isset($_POST['name'])) {
        $req = $pdo->prepare("update contacts set name = :name,contact = :contact,email = :email, image = :image where id= :id");
        $req->execute(['name' => $_POST['name'],
            'contact' => $_POST['contact'],
            'email' => $_POST['email'],
            'image' => $userPic,
            'id' => $id,
        ]);

        header("Location: index.php");
    }
}

?>
<?php require 'inc/header.php'?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Modifier un contact
                </div>
                <div class="card-body">
                    <form class="" action="" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="name">Nom</label>
                            <input type="text" class="form-control" name="name"  placeholder="Enter Name" value="<?php echo $contact['name']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="contact">Tél</label>
                            <input type="text" class="form-control" name="contact" placeholder="Enter Mobile Number" value="<?php echo $contact['contact']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="email">E-Mail</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter Email" value="<?php echo $contact['email']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="image">Choisir image</label>
                            <div class="col-md-6">
                                <img src="<?php echo $upload_dir.$contact['image'] ?>" class="card-img-top">
                                <input type="file" class="form-control" name="image" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="Submit" class="btn btn-warning waves">Modifier</button>
                        </div>
                    </form>
                </div>
            </div>
            <a class="btn btn-outline-primary mt-3" href="index.php"><i class="fa fa-sign-out-alt"></i><span>Back</span></a>


        </div>
    </div>
</div>
<?php require 'inc/footer.php'?>

<script src="js/bootstrap.min.js" charset="utf-8"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" charset="utf-8"></script>
</body>
</html>
