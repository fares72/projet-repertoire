<?php
include('add.php');
require_once 'inc/functions.php';
logged_only();
?>

<?php require 'inc/header.php'?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Ajouter un contact</div>
                <div class="card-body">
                    <form class="" action="add.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="name">Nom</label>
                            <input type="text" class="form-control" name="name"  placeholder="Enter Name" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="contact">Tél</label>
                            <input type="text" class="form-control" name="contact" placeholder="Enter Mobile Number" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="email">E-Mail</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter Email" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="image">Choisir image</label>
                            <input type="file" class="form-control" name="image" value="" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="Submit" class="btn btn-success waves">Ajouter</button>
                        </div>
                    </form>
                </div>
            </div>
            <a class="btn btn-outline-primary mt-3" href="index.php"><i class="fa fa-sign-out-alt"></i><span>Back</span></a>
        </div>
    </div>
</div>

<script src="js/bootstrap.min.js" charset="utf-8"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" charset="utf-8"></script>
</body>
</html>

