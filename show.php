<?php
require_once('inc/db.php');
require 'inc/functions.php';
logged_only();
$upload_dir = 'uploads/';

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $req = $pdo->prepare("SELECT * FROM contacts WHERE id= :id AND user_id = :user_id");
    $req->execute(['id' => $id,
                    'user_id' => $_SESSION['auth']->id,
    ]);
    $contact=$req->fetch(PDO::FETCH_ASSOC);
}
?>

<?php require 'inc/header.php'?>

<div class="container shadow-lg p-3 mb-5 bg-white rounded">
    <div class="row justify-content-center">
        <div class="col md-9">
        <div class="card border-0">

            <div class="card-body">
                <div class="row">
                    <div class="col-md">
                        <img src="<?php echo $upload_dir.$contact['image'] ?>" class="card-img-top">
                    </div>
                    <div class="col-md">
                        <h5 class="form-control"><i class="fa fa-user-tag">
                                <span><?php echo $contact['name'] ?></span>
                            </i></h5>
                        <h5 class="form-control"><i class="fa fa-mobile-alt">
                                <span><?php echo $contact['contact'] ?></span>
                            </i></h5>
                        <h5 class="form-control"><i class="fa fa-envelope">
                                <span><?php echo $contact['email'] ?></span>
                            </i></h5>

                        <a class="btn btn-outline-primary" href="index.php"><i class="fa fa-sign-out-alt"></i><span>Back</span></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>

<?php require 'inc/footer.php'?>
